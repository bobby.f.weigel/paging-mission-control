package com.houstonwehaveaproblem.pagingmissioncontrol.enums;

public enum SatelliteSeverityEnum {
    RH("RED HIGH"), RL ("RED LOW"), YH ("YELLOW HIGH"), YL("YELLOW LOW");

    public final String severity;

    SatelliteSeverityEnum(String severity) {
        this.severity = severity;
    }
}
