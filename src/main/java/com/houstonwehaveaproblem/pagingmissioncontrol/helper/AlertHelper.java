package com.houstonwehaveaproblem.pagingmissioncontrol.helper;

import com.houstonwehaveaproblem.pagingmissioncontrol.domain.AlertCount;
import com.houstonwehaveaproblem.pagingmissioncontrol.domain.AlertMessage;
import com.houstonwehaveaproblem.pagingmissioncontrol.domain.SatelliteData;
import com.houstonwehaveaproblem.pagingmissioncontrol.enums.SatelliteSeverityEnum;
import com.houstonwehaveaproblem.pagingmissioncontrol.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class AlertHelper {
    Logger logger = LoggerFactory.getLogger(AlertHelper.class);

    public final static int MAX_VIOLATION_ALERT_LIMIT = 3;

    public List<AlertMessage> createAlertList(AlertCount alertCount, SatelliteData data) {
        List<AlertMessage> alertList = new ArrayList<>();
        try {
            // prioritize red alerts! Only create 1 alert even if multiple categories are over the violation limit
            if (alertCount.redHighAlertCount >= MAX_VIOLATION_ALERT_LIMIT) {
                alertList.add(createFormattedAlert(data.getSatelliteId(), SatelliteSeverityEnum.RH, data.getComponent(), data.getTimeStamp()));
            } else if (alertCount.redLowAlertCount >= MAX_VIOLATION_ALERT_LIMIT) {
                alertList.add(createFormattedAlert(data.getSatelliteId(), SatelliteSeverityEnum.RL, data.getComponent(), data.getTimeStamp()));
            } else if (alertCount.yellowHighAlertCount >= MAX_VIOLATION_ALERT_LIMIT) {
                alertList.add(createFormattedAlert(data.getSatelliteId(), SatelliteSeverityEnum.YH, data.getComponent(), data.getTimeStamp()));
            } else if (alertCount.yellowLowAlertCount >= MAX_VIOLATION_ALERT_LIMIT) {
                alertList.add(createFormattedAlert(data.getSatelliteId(), SatelliteSeverityEnum.YL, data.getComponent(), data.getTimeStamp()));
            }
        } catch (ParseException e) {
            logger.error("unable to create alert list: " + e.getStackTrace());
        }
        return alertList;
    }

    /*
    Looks at specific satellite data to see if the satellite raw data violates max/min limits. Can't go over MAX_VIOLATION_ALERT_LIMIT value
     */
    public void countVioloations(AlertCount count, SatelliteData data) {
        // don't do else/if. May have 3 yellow limit violations and only 1 red violation
        if(data.getRawValue() > data.getRedHighLimit()) count.redHighAlertCount++;
        if(data.getRawValue() > data.getYellowHighLimit()) count.yellowHighAlertCount++;
        if(data.getRawValue() < data.getRedLowLimit()) count.redLowAlertCount++;
        if(data.getRawValue() < data.getYellowLowLimit()) count.yellowLowAlertCount++;
    }
    private AlertMessage createFormattedAlert(Long satelliteId, SatelliteSeverityEnum severity, String component, Date timestamp) throws ParseException {
        return new AlertMessage().builder()
                .component(component)
                .timestamp(DateUtil.getDisplayDate(timestamp))
                .satelliteId(satelliteId)
                .severity(severity.severity)
                .build();
    }

}
