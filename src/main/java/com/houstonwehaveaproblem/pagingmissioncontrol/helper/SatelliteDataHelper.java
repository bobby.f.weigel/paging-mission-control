package com.houstonwehaveaproblem.pagingmissioncontrol.helper;

import com.houstonwehaveaproblem.pagingmissioncontrol.domain.AlertCount;
import com.houstonwehaveaproblem.pagingmissioncontrol.domain.AlertMessage;
import com.houstonwehaveaproblem.pagingmissioncontrol.domain.SatelliteData;
import com.houstonwehaveaproblem.pagingmissioncontrol.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class SatelliteDataHelper {
    private AlertHelper alertHelper;

    @Autowired
    public SatelliteDataHelper(AlertHelper alertHelper) {
        this.alertHelper = alertHelper;
    }

    /*
    Iterates through the entire list of satellite data to generate alerts
    Responsible for keeping track of satellite IDs and components that have already been analyzed
    For everything that has not been analyzed, sends them through the analyze method
     */
    public ArrayList analyzeSatelliteData(List<SatelliteData> satelliteDataArrayList) {
        Map coveredComponents = new HashMap(); //map of alerts already covered
        ArrayList alertList = new ArrayList(); // list of alerts we will send back to the user

        for(SatelliteData sa : satelliteDataArrayList) {
            SatelliteData satelliteData = (SatelliteData) coveredComponents.get(sa.getSatelliteId()+sa.getComponent());
            if(satelliteData == null) {
                coveredComponents.put(sa.getSatelliteId()+sa.getComponent(), sa); // unique entry. place in map to be checked against so we don't analyze items twice

                //filter out everything with the same satelliteID and component, then check through them to gather all alerts
                List<SatelliteData> list = satelliteDataArrayList.stream()
                        .filter(data -> data.getSatelliteId() == sa.getSatelliteId()
                                && data.getComponent().equals(sa.getComponent()))
                        .collect(Collectors.toList());
                alertList.addAll(analyze(list));
            }
        }
        return alertList;
    }
    /*
    Will look through a list of Satellite Data and will create alert messages based off of that data.
    Will create a 5 minute window and slide that window across the entire data set to find alerts
     */
    private List<AlertMessage> analyze(List<SatelliteData> list) {
        List<AlertMessage> alertList = new ArrayList<>();
        if(list.size() >= 3) {
            for (int currentDataIndex = 0; currentDataIndex < list.size(); currentDataIndex++) {
                AlertCount alertCount = new AlertCount();
                alertHelper.countVioloations(alertCount, list.get(currentDataIndex));
                for (int dataWithin5Min = currentDataIndex + 1; dataWithin5Min < list.size() && TimeUtil.isBelowFiveMinutes(list.get(currentDataIndex), list.get(dataWithin5Min)); dataWithin5Min++) {
                    alertHelper.countVioloations(alertCount, list.get(dataWithin5Min));
                }
                alertList.addAll(alertHelper.createAlertList(alertCount, list.get(currentDataIndex)));
            }
        }

        return alertList;
    }

}
