package com.houstonwehaveaproblem.pagingmissioncontrol.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static Date getTimestampFromString(String stringTimeStamp) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        Date parsedDate = dateFormat.parse(stringTimeStamp);
        return parsedDate;
    }
    public static String getDisplayDate(Date date) throws ParseException {
        SimpleDateFormat newformat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        return newformat.format(date);
    }

}
