package com.houstonwehaveaproblem.pagingmissioncontrol.util;

import com.houstonwehaveaproblem.pagingmissioncontrol.domain.SatelliteData;

import java.util.concurrent.TimeUnit;

public class TimeUtil {
    public static boolean isBelowFiveMinutes(SatelliteData start, SatelliteData end) {
        long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(end.getTimeStamp().getTime() - start.getTimeStamp().getTime());
        return diffMinutes <= 5;
    }
}
