package com.houstonwehaveaproblem.pagingmissioncontrol.domain;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Data
@EqualsAndHashCode
public class AlertMessage{
    private long satelliteId;
    private String severity;
    private String component;
    private String timestamp;

}
