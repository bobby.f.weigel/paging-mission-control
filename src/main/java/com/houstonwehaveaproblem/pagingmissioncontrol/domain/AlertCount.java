package com.houstonwehaveaproblem.pagingmissioncontrol.domain;

import lombok.ToString;

@ToString
public class AlertCount {
    public int redHighAlertCount;
    public int redLowAlertCount;
    public int yellowHighAlertCount;
    public int yellowLowAlertCount;

    public AlertCount() {
        this.redHighAlertCount = 0;
        this.redLowAlertCount = 0;
        this.yellowHighAlertCount = 0;
        this.yellowLowAlertCount = 0;
    }
}
