package com.houstonwehaveaproblem.pagingmissioncontrol.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@ToString
@Table(name="satellitedata")
public class SatelliteData implements Serializable {
    private long entryId;
    private long satelliteId;
    private Date timeStamp;
    private double redHighLimit;
    private double yellowHighLimit;
    private double yellowLowLimit;
    private double redLowLimit;
    private double rawValue;
    private String component;

    public SatelliteData(long satelliteId, Date timeStamp, double redHighLimit, double yellowHighLimit, double yellowLowLimit,   double redLowLimit,  double rawValue, String component ) {
        this.component = component;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this. yellowLowLimit = yellowLowLimit;
        this.satelliteId = satelliteId;
        this.timeStamp = timeStamp;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public long getEntryId() {
        return entryId;
    }

    public void setEntryId(long entryId) {
        this.entryId = entryId;
    }
    public long getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(long satelliteId) {
        this.satelliteId = satelliteId;
    }
    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(double redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(double yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public double getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(double yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public double getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(double redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(double rawValue) {
        this.rawValue = rawValue;
    }
    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

}
