package com.houstonwehaveaproblem.pagingmissioncontrol.controller;

import com.houstonwehaveaproblem.pagingmissioncontrol.service.SatelliteDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class SatelliteDataController {

    private final SatelliteDataService satelliteDataService;

    @Autowired
    public SatelliteDataController(SatelliteDataService satelliteDataService) {
        this.satelliteDataService = satelliteDataService;
    }

    @PostMapping("/receive")
    public ResponseEntity receiveSatelliteData(@RequestParam("file") MultipartFile file) {
        ResponseEntity satelliteDataResponse = null;

        if (!file.isEmpty()) {
            satelliteDataResponse = this.satelliteDataService.process(file);
        } else {
            satelliteDataResponse = new ResponseEntity(HttpStatus.OK);
        }

        return satelliteDataResponse;
    }
}
