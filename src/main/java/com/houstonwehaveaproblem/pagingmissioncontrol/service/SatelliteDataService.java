package com.houstonwehaveaproblem.pagingmissioncontrol.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;


public interface SatelliteDataService {
    ResponseEntity process(MultipartFile file);
}
