package com.houstonwehaveaproblem.pagingmissioncontrol.service;


import com.houstonwehaveaproblem.pagingmissioncontrol.dao.SatelliteDataRepository;
import com.houstonwehaveaproblem.pagingmissioncontrol.domain.SatelliteData;
import com.houstonwehaveaproblem.pagingmissioncontrol.helper.SatelliteDataHelper;
import com.houstonwehaveaproblem.pagingmissioncontrol.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class SatelliteDataServiceImpl implements SatelliteDataService {
    Logger logger = LoggerFactory.getLogger(SatelliteDataServiceImpl.class);
    private SatelliteDataRepository satelliteDataRepository;
    private SatelliteDataHelper dataHelper;

    @Autowired
    public SatelliteDataServiceImpl(SatelliteDataRepository satelliteDataRepository, SatelliteDataHelper dataHelper) {
        this.satelliteDataRepository = satelliteDataRepository;
        this.dataHelper = dataHelper;
    }

    @Override
    public ResponseEntity process(MultipartFile file) {
        // Retrieve the last 5 minutes of information just in case we had to restart
        List<SatelliteData> satelliteDataArrayList = this.satelliteDataRepository.findLastFiveMinutesData();

        ResponseEntity response = new ResponseEntity<>( HttpStatus.OK);

        try {
            String content = new String(file.getBytes(), StandardCharsets.UTF_8);
            String lines[] = content.split("\\r?\\n");

            for(String line : lines) {
                String[] satelliteData = line.split("\\|");
                SatelliteData data = this.saveSatelliteData(satelliteData);
                satelliteDataArrayList.add(data);
            }

            //order by timestamp and only include stuff that's above or below the yellow limits so we only get potential alert items
            List<SatelliteData> sortedList = satelliteDataArrayList.stream()
                    .filter(c -> c.getRawValue() > c.getYellowHighLimit() || c.getRawValue() < c.getYellowLowLimit())
                    .sorted(Comparator.comparing(SatelliteData::getTimeStamp))
                    .distinct()
                    .collect(Collectors.toList());

            ArrayList alertList = dataHelper.analyzeSatelliteData(sortedList);
            if(!alertList.isEmpty()) {
                response = new ResponseEntity<>(alertList, HttpStatus.OK);
            }

        } catch (Exception e) {
            logger.error("unable to analyze data + \n" + e.getStackTrace());
            response = new ResponseEntity<>( "Unable to analyze file", HttpStatus.BAD_REQUEST);
        } finally {
            return response;
        }

    }


    public SatelliteData saveSatelliteData(String[] satelliteData) throws ParseException {
        SatelliteData data = SatelliteData.builder()
                .timeStamp(DateUtil.getTimestampFromString(satelliteData[0]))
                .satelliteId(new Long(satelliteData[1]))
                .redHighLimit(new Double(satelliteData[2]))
                .yellowHighLimit(new Double(satelliteData[3]))
                .yellowLowLimit(new Double(satelliteData[4]))
                .redLowLimit(new Double(satelliteData[5]))
                .rawValue(new Double(satelliteData[6]))
                .component(satelliteData[7])
                .build();
        try {
            satelliteDataRepository.save(data);
        } catch (DataAccessException dae) {
            logger.error("unable to save data + \n" + dae.getStackTrace());
        } finally {
            return data;
        }
    }
}
