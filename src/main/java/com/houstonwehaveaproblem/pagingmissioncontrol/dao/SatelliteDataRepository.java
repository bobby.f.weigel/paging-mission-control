package com.houstonwehaveaproblem.pagingmissioncontrol.dao;

import com.houstonwehaveaproblem.pagingmissioncontrol.domain.SatelliteData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/*
Probably should move away from an in-memory database
 */
public interface SatelliteDataRepository extends CrudRepository<SatelliteData, Long> {
    // Select everything within 5 minutes of current time. .003472222 represents 5 minutes
    @Query("SELECT new SatelliteData (sd.satelliteId, sd.timeStamp, sd.redHighLimit, sd.yellowHighLimit, sd.yellowLowLimit, sd.redLowLimit, sd.rawValue, sd.component ) FROM SatelliteData sd WHERE sd.timeStamp >= NOW() - .003472222")
    List<SatelliteData> findLastFiveMinutesData();
}
