package com.houstonwehaveaproblem.pagingmissioncontrol;

import com.houstonwehaveaproblem.pagingmissioncontrol.domain.AlertMessage;
import com.houstonwehaveaproblem.pagingmissioncontrol.service.SatelliteDataService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;

@SpringBootTest
public class SatelliteDataServiceTests {
    @Autowired
    private SatelliteDataService satelliteDataService;


    @Test
    public void testBaseCase() {
        AlertMessage firstMessage = new AlertMessage().builder()
                .satelliteId(1000)
                .component("BATT")
                .severity("RED LOW")
                .timestamp("20180101 23:01:09.521")
                .build();
        AlertMessage secondMessage = new AlertMessage().builder()
                .satelliteId(1000)
                .component("TSTAT")
                .severity("RED HIGH")
                .timestamp("20180101 23:01:38.001")
                .build();


        try {
            File file = new ClassPathResource(
                    "static/basecase.txt").getFile();
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file",
                    file.getName(), "text/plain", IOUtils.toByteArray(input));
            ResponseEntity entity = satelliteDataService.process(multipartFile);
            ArrayList<AlertMessage> list = (ArrayList) entity.getBody();

            Assert.isTrue(list.size() == 2, "Size must be 2");

            Assert.isTrue(list.get(0).equals(firstMessage)
                    , "First element should be: " + firstMessage.toString() + ", but actually was : " + list.get(0));

            Assert.isTrue(list.get(1).equals(secondMessage)
                    , "Second element should be: " + secondMessage.toString() + ", but actually was : " + list.get(1));


        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Test
    public void testDifferentYears() {
        AlertMessage firstMessage = new AlertMessage().builder()
                .satelliteId(1001)
                .component("TSTAT")
                .severity("RED HIGH")
                .timestamp("20201231 23:59:05.001")
                .build();
        AlertMessage secondMessage = new AlertMessage().builder()
                .satelliteId(1001)
                .component("TSTAT")
                .severity("YELLOW HIGH")
                .timestamp("20210101 13:01:38.001")
                .build();
        AlertMessage thirdMessage = new AlertMessage().builder()
                .satelliteId(1001)
                .component("TSTAT")
                .severity("YELLOW HIGH")
                .timestamp("20210101 13:02:49.021")
                .build();

        try {
            File file = new ClassPathResource(
                    "static/differentyears.txt").getFile();
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file",
                    file.getName(), "text/plain", IOUtils.toByteArray(input));
            ResponseEntity entity = satelliteDataService.process(multipartFile);
            ArrayList<AlertMessage> list = (ArrayList) entity.getBody();

            Assert.isTrue(list.size() == 3, "Size must be 3");

            Assert.isTrue(list.get(0).equals(firstMessage)
                    , "First element should be: " + firstMessage.toString() + ", but actually was : " + list.get(0));

            Assert.isTrue(list.get(1).equals(secondMessage)
                    , "second element should be: " + secondMessage.toString() + ", but actually was : " + list.get(1));

            Assert.isTrue(list.get(2).equals(thirdMessage)
                    , "third element should be: " + thirdMessage.toString() + ", but actually was : " + list.get(2));

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }
    @Test
    public void testYellowIndicators() {
        AlertMessage firstMessage = new AlertMessage().builder()
                .satelliteId(1000)
                .component("BATT")
                .severity("YELLOW LOW")
                .timestamp("20180101 23:01:09.521")
                .build();
        AlertMessage secondMessage = new AlertMessage().builder()
                .satelliteId(1000)
                .component("TSTAT")
                .severity("RED HIGH")
                .timestamp("20180101 23:01:38.001")
                .build();
        AlertMessage thirdMessage = new AlertMessage().builder()
                .satelliteId(1001)
                .component("BATT")
                .severity("YELLOW LOW")
                .timestamp("20180101 23:03:11.531")
                .build();

        try {
            File file = new ClassPathResource(
                    "static/yellowalerts.txt").getFile();
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file",
                    file.getName(), "text/plain", IOUtils.toByteArray(input));
            ResponseEntity entity = satelliteDataService.process(multipartFile);
            ArrayList<AlertMessage> list = (ArrayList) entity.getBody();

            Assert.isTrue(list.size() == 3, "Size must be 3");

            Assert.isTrue(list.get(0).equals(firstMessage)
                    , "First element should be: " + firstMessage.toString() + ", but actually was : " + list.get(0));

            Assert.isTrue(list.get(1).equals(secondMessage)
                    , "second element should be: " + secondMessage.toString() + ", but actually was : " + list.get(1));

            Assert.isTrue(list.get(2).equals(thirdMessage)
                    , "third element should be: " + thirdMessage.toString() + ", but actually was : " + list.get(2));

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
